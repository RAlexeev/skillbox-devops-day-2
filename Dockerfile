# Base image which contains global dependencies
FROM node

RUN mkdir /flatris
COPY package.json /flatris
WORKDIR /flatris
RUN yarn install

COPY . /flatris
RUN yarn test
RUN yarn build

# Run the App
CMD yarn start

# Open the network port to have an external access to the App
EXPOSE 3000
